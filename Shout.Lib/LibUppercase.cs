﻿using System;

namespace Shout.Lib
{
    public static class LibUppercase
    {
        public static string Uppercase(string text)
        {
            return text.ToUpper();
        }
    }
}
