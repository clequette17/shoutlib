using NUnit.Framework;

namespace Shout.Lib.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestUppercase()
        {
            string result = LibUppercase.Uppercase("test");
            string expected = "TEST";

            Assert.AreEqual(expected, result);
        }
    }
}